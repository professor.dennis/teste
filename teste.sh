#!/bin/bash

# SCRIPT SHELL para migrar versão Joomla para 3.7
# Divisão de Portais Web - spw@tic.ufrj.br
# TIC/UFRJ - Todos os direitos reservados
# Criado em: 08/05/2017
# ----------------------------------------------
# Use: ./migra_37

# variáveis globais ----------------------------

# local onde se espera encontrar as instâncias Joomla
BASEPATH="/var/www/html/area_script";
# local onde ficarão armazenados todos os arquivos de backup de dump das instâncias processadas. ex. /home/ticadmin
PATHBACKUP="/home/dennis/Downloads";
# localização do arquivo contendo o pacote da versão 3.7 do Joomla
FILESTRU=`find ${PATHBACKUP} -type f -name 'Joomla_3.7.0-Stable-Full_Package.tar.gz'`;
# Nome do arquivo contido no dump original (padrão)
ARQUIVO='joomla.sql';
# Nome do arquivo base gerado
MODFILE='migra.sql';
# Local de extração padrão do TAR
SUBDIR='installation/sql/mysql/';
# Prefixo padrão das tabelas contidas no dump original
PREFIX='#__';
# Coletando a data atual para usar como parte do nome do arquivo de backup
DATE=`date +%Y-%m-%d`;
# Detectando a versão Joomla existente
COMANDO=`find ${BASEPATH} -type f -name 'configuration.php' ! -wholename '**/administrator'`;

# Cabeçalho inicial
clear;
echo "***** SCRIPT SHELL para migrar versão Joomla para 3.7 *******";
echo "*************************************************************";
echo "*** AGUARDE. Detectando Joomlas no local especificado...";
echo "*************************************************************";
sleep 2;


# Bloco de detecção de versão
CONT=0;
for L in ${COMANDO} ;
do
  D=`dirname $L` ;
  DIR=$D/portal/language/en-GB/en-GB.xml;
  DIRE=$D/language/en-GB/en-GB.xml;
	if [[ -e "$DIR" ]] || [[ -e "$DIRE" ]] ; then
		if [[ -e "$DIR" ]] ; then
			RDIR=$DIR ;
			VERSAO=`grep 'version' $RDIR | sed '{3! d;s/<[^>]*>//g}'` ;
			VERSAOFINAL=`echo $VERSAO | cut -d "." -f 1`;
			#echo "[Achei JOOMLA] >> em $D na versão $VERSAO" ;
		elif [[ -e "$DIRE" ]] ; then
			RDIR=$DIRE ;
			VERSAO=`grep 'version' $RDIR | sed '{3! d;s/<[^>]*>//g}'` ;
			VERSAOFINAL=`echo $VERSAO | cut -d "." -f 1`;
			#echo "[Achei JOOMLA] >> em $D na versão $VERSAO" ;
		fi;
		PATHFOUND[$CONT]=$D;
		VERSIONFOUND[$CONT]=$VERSAOFINAL;
		CONT=$(($CONT+1));
	fi;
done;

TOTAL=$CONT;
clear;
echo "***** SCRIPT SHELL para migrar versão Joomla para 3.7 *******";
echo "[ Aguarde... Processando instâncias de zero até $TOTAL... ]";
sleep 2;

# Início do processamento das N instâncias encontradas
INSTANCIA=0;
while [ $INSTANCIA -lt $TOTAL ]
do
	# Configurações --------------------
  CAPTURA=${VERSIONFOUND[$INSTANCIA]}; # Variável que captura qualquer versão 3 detectada
  LOCAL=${PATHFOUND[$INSTANCIA]};      # Local onde serão armazenados os arquivos e pastas zipados
	MYSQL=$(which mysql);
	AWK=$(which awk);
	GREP=$(which grep);
	MYSQLDUMP=$(which mysqldump);
	if [ $CAPTURA -eq 3 ]; then
    # variáveis locais
    BD=`grep '$db = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    USUARIO=`grep '$user = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    PASS=`grep '$password = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    PREFIXB=`grep '$dbprefix =' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    # Para uso futuro...
    # FROMNAME=`grep '$fromname =' configuration.php | cut -d "'" -f 2` ;
    # METADESC=`grep '$MetaDesc =' configuration.php | cut -d "'" -f 2` ;
    # METAKEYS=`grep '$MetaKeys =' configuration.php | cut -d "'" -f 2` ;
    # LOGPATH=`grep '$log_path =' configuration.php | cut -d "'" -f 2` ;
    # TMPPATH=`grep '$tmp_path =' configuration.php | cut -d "'" -f 2` ;
    # SITENAME=`grep '$sitename =' configuration.php | cut -d "'" -f 2` ;
    # Verificação se existem tabelas no banco de dados desta instância
    TABELAS=$($MYSQL -u $USUARIO -p$PASS $BD -e 'show tables'| $AWK '{ print $1}' | $GREP -v '^Tables' );
    # Local temporário para guardar os arquivos compactados e base SQL
    TMPFILES=${PATHBACKUP}"/temporaryfiles";
    # Recriando o diretório temporário se ele já existir
    if [ -d "$TMPFILES" ]; then
        rm -rf $TMPFILES;
        mkdir $TMPFILES; # Recriando
    else
        mkdir $TMPFILES; # Fazendo pela primeira vez
    fi;
    if [ "$TABELAS" == "" ] ; then
		  echo "[ AVISO: A instância #$INSTANCIA não possui tabelas no banco de dados. ]";
      sleep 2;
    else
      echo "[ Processando instância $INSTANCIA em $LOCAL ]";
      echo "------------------------------------------------------------------------";
      sleep 2;
      # ------------------------------------------------------------------------------------------------------------------------------ REMOVER
      # Flag para configuração dos arquivos SQL utilizados
      ENDSCRIPT="NAO";
      if [ "$ENDSCRIPT" == "NAO" ] ; then                                         # Criação dos padrões necessários
        echo `tar -vxf $FILESTRU -C $TMPFILES $SUBDIR$ARQUIVO`;                   # Descompacta "joomla.sql" em sua árvore original
        echo `mv $TMPFILES/$SUBDIR$ARQUIVO $TMPFILES`;                            # Move o arquivo "joomla.sql" para o diretório temporário
        echo `rm -rf $TMPFILES/$SUBDIR`;                                          # Apaga a árvore original
        echo `sed s/$PREFIX/$PREFIXB/g $TMPFILES/$ARQUIVO > $TMPFILES/$MODFILE`;  # Clonagem de "Joomla.sql" para "migra.sql" já com prefixos corretos
        # Criação de arquivos temporários de cada instância
        DESTINO=$TMPFILES"/tabelas.txt";                                          # lista de tabelas que devem ser reservadas da versão antiga
        EXPORT=$TMPFILES"/export.sql";                                            # dump de parte das tabelas da nova versão, excluindo-se as tabelas da versão antiga
        TRUNCATELIST=$TMPFILES"/truncatelist.txt";                                # lista das tabelas originais que serão truncadas
        TABELASATUAIS=$TMPFILES"/tabelasatuais.txt";                              # nome para sobrescrita da lista de tabelas separada por vírgulas
        TABELASEXCLUIDAS=$TMPFILES"/tabelasexcluidas.txt";                        # lista de tabelas antigas a excluir
        BACKUP="/backup_"$BD"_"$DATE".sql";                                       # nome do arquivo de backup
        ENDSCRIPT="SIM";
      fi;
      # Criando lista das tabelas que serão injetadas no arquivo TABELAS.TXT
      echo "[ Passo 1 de 13 ] - Criando índice de tabelas necessárias...";
      echo "===============================================================================";
      echo $PREFIXB"user_profiles" > $DESTINO;
      echo $PREFIXB"usergroups" >> $DESTINO;
      echo $PREFIXB"user_usergroup_map" >> $DESTINO;
      echo $PREFIXB"users" >> $DESTINO;
      echo $PREFIXB"menu" >> $DESTINO;
      echo $PREFIXB"menu_types" >> $DESTINO;
      echo $PREFIXB"modules" >> $DESTINO;
      echo $PREFIXB"modules_menu" >> $DESTINO;
      echo $PREFIXB"content_types" >> $DESTINO;
      echo $PREFIXB"content" >> $DESTINO;
      echo $PREFIXB"contact_details" >> $DESTINO;
      echo $PREFIXB"categories" >> $DESTINO;
      echo $PREFIXB"assets" >> $DESTINO;
      echo $PREFIXB"extensions" >> $DESTINO;
      echo $PREFIXB"template_styles" >> $DESTINO;
      echo $PREFIXB"viewlevels" >> $DESTINO;
      sleep 5;
      clear;
      # Tela ---------------------------------------------
      echo "[ Passo 2 de 13 ] - Criando o dump do banco de dados atual...";
      echo "=============================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO [OK]";
      sleep 5;
      clear;
      # Fazer o backup total do original e reservando-o
      $MYSQLDUMP -u $USUARIO -p$PASS $BD > ${PATHBACKUP}$BACKUP;
      clear;
      # Tela ---------------------------------------------
      echo "[ Passo 3 de 13 ] - Coletando de algumas tabelas, os dados importantes...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      sleep 5;
      clear;
      # Coletando algumas tabelas desse banco
      BUSCA=`cat $DESTINO`;
      INDICE=0;
      for i in $BUSCA
      do
       # Vetor
       INDICE=$(($INDICE+1));
       VETOR[$INDICE]=$i;
       TAMVETOR=${#VETOR[@]};
      done;
      # Coleta concluída
      $MYSQLDUMP -u $USUARIO -p$PASS $BD ${VETOR[@]:0:$INDICE} > $EXPORT;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 4 de 13 ] - Removendo todas as tabelas atuais...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      sleep 5;
      clear;
      # Apagando TODAS as tabelas
      echo $TABELAS >> $TABELASATUAIS;
      EXCLUSAO=`cat $TABELASATUAIS | tr ' ' ',' > $TABELASEXCLUIDAS`;
      EXCLUIDAS=`cat $TABELASEXCLUIDAS`;
      if [ "$EXCLUIDAS" == "" ]; then
          echo "[ ERRO. Não é possível ler o arquivo $TABELASEXCLUIDAS. Abortando... ";
          exit 1;
      fi;
      X=0;
      for f in $EXCLUIDAS
      do
        X=$(($X+1));
        VET[$X]=$f;
      done;
      while [ $X -gt 0 ]
      do
        echo "[ Aguarde... Apagando tabelas da instância: $X em $LOCAL ]";
        #echo "Tabelas atuais: ${VET[$X]:0};";
        $MYSQL -s -u $USUARIO -p$PASS $BD -e "DROP TABLE ${VET[$X]:0};";
        X=$(($X-1));
      done;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 5 de 13 ] - Recriando no banco a nova estrutura de tabelas.";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      sleep 5;
      clear;
      # Criando a nova estrutura sem as tabelas da lista
      FILE=`find ${TMPFILES} -type f -name $MODFILE`;    # localização do arquivo base gerado por este script (DEVE SER O MESMO EM MODFILE)
      if [ "$FILE" != "" ] ; then
            $MYSQL -u $USUARIO -p$PASS $BD < $FILE;
      fi;
      if [ "$FILE" == "" ] ; then
            echo "ERRO ! O arquivo requerido $MODFILE está faltando para executar este passo ! Abortando...";
            exit 2;
      fi;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 6 de 13 ] - Truncando as tabelas originais...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados."
      sleep 5;
      clear;
      # Preparação para truncar tabelas
      if [ -e $TRUNCATELIST ] ; then
          rm -rf $TRUNCATELIST;
      fi;
      echo $PREFIXB"user_profiles" > $TRUNCATELIST;
      echo $PREFIXB"usergroups" >> $TRUNCATELIST;
      echo $PREFIXB"user_usergroup_map" >> $TRUNCATELIST;
      echo $PREFIXB"users" >> $TRUNCATELIST;
      echo $PREFIXB"menu" >> $TRUNCATELIST;
      echo $PREFIXB"menu_types" >> $TRUNCATELIST;
      echo $PREFIXB"modules" >> $TRUNCATELIST;
      echo $PREFIXB"modules_menu" >> $TRUNCATELIST;
      echo $PREFIXB"content_types" >> $TRUNCATELIST;
      echo $PREFIXB"content" >> $TRUNCATELIST;
      echo $PREFIXB"contact_details" >> $TRUNCATELIST;
      echo $PREFIXB"categories" >> $TRUNCATELIST;
      echo $PREFIXB"assets" >> $TRUNCATELIST;
      echo $PREFIXB"extensions" >> $TRUNCATELIST;
      echo $PREFIXB"template_styles" >> $TRUNCATELIST;
      echo $PREFIXB"viewlevels" >> $TRUNCATELIST;
      # truncando as tabelas originais
      if [ -e "$TRUNCATELIST" ]; then
           TRUNCLIST=`cat $TRUNCATELIST`;
           X=0;
           for f in $TRUNCLIST
           do
               X=$(($X+1));
               VET[$X]=$f;
           done;
           Z=$X;
           while [ $X -gt 0 ]
           do
               echo "[Aguarde... Truncando tabelas originais da instância. Restam: $X/$Z ]";
               echo "Tabela atual: ${VET[$X]:0};";
               echo `$MYSQL -s -u $USUARIO -p$PASS $BD -e "TRUNCATE TABLE ${VET[$X]:0};"`;
               X=$(($X-1));
               sleep 2;
               clear;
           done;
      fi;
      sleep 5;
      clear;
      # Recolocando as tabelas da lista na nova estrutura
      # Tela -----------------------------------------
      echo "[ Passo 7 de 13 ] - Recolocando os dados da instância na nova estrutura...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados.";
      echo "[6] - Truncadas as tabelas originais.";
      sleep 5;
      clear;
      if [ -e "$EXPORT" ]; then
          echo "[Aguarde... Subindo o arquivo SQL... ]";
          $MYSQL -u $USUARIO -p$PASS $BD < $EXPORT;
          sleep 2;
          clear;
      fi;
      # Tela -----------------------------------------
      echo "[ Passo 8 de 13 ] - Removendo arquivos temporários...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados."
      echo "[6] - Truncadas as tabelas originais.";
      echo "[7] - Recolocado em $BD os dados guardados.";
      sleep 5;
      clear;
      # limpando arquivos temporários (parte de banco de dados)
      if [ -e "$DESTINO" ] ; then rm -rf "$DESTINO"; fi;
      if [ -e "$EXPORT" ] ; then rm -rf "$EXPORT"; fi;
      if [ -e "$TRUNCATELIST" ] ; then rm -rf "$TRUNCATELIST"; fi;
      if [ -e "$TABELASATUAIS" ] ; then rm -rf "$TABELASATUAIS"; fi;
      # limpando arquivos temporários (parte estrutural - 10 arquivos)
      if [ -e "$TMPFILES/${DESTFILE[1]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[1]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[2]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[2]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[3]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[3]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[4]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[4]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[5]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[5]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[6]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[6]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[7]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[7]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[8]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[8]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[9]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[9]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[10]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[10]:0}"; fi;
      # Nome dos arquivos usados na preservação de parte da estrutura original
      DESTFILE[1]="administrator.tar";
      DESTFILE[2]="components.tar";
      DESTFILE[3]="images.tar";
      DESTFILE[4]="language_pt.tar";
      DESTFILE[5]="language_en.tar";
      DESTFILE[6]="media.tar";
      DESTFILE[7]="modules.tar";
      DESTFILE[8]="plugins.tar";
      DESTFILE[9]="templates.tar";
      DESTFILE[10]="extras.tar";
      # Verificação do diretório temporário
      if [ -d $TMPFILES ]; then
      	echo "Compactando...";
      else
      	mkdir $TMPFILES;
      fi;
      if [ -d $LOCAL ]; then
        echo "[Aguarde... Verificando arquivos e pastas para compactar... ]";
      	sleep 2;
      	Q=0;
        # administrator.tar
      	if [ -d $LOCAL'/administrator/components/com_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/components/com_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/administrator/components/com_jce' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/components/com_jce";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/administrator/language/pt-BR' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/language/pt-BR";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/manifests/packages/pkg_pt-BR.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/manifests/packages/pkg_pt-BR.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/manifests/packages/pkg_weblinks.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/manifests/packages/pkg_weblinks.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/templates/bluestork' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/templates/bluestork";
      			Q=$(($Q+1));
      		fi;
      	fi; # Compactado o arquivo administrator.tar
        #components.tar
      	if [ -d $LOCAL'/components/com_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[2]:0} --absolute-names $LOCAL/components/com_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/components/com_jce' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[2]:0} --absolute-names $LOCAL/components/com_jce";
      			Q=$(($Q+1));
      		fi;
      	fi; # Compactado o arquivo componentes.tar
        #images.tar
      	if [ -d $LOCAL'/images' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[3]:0} --absolute-names $LOCAL/images";
      		Q=$(($Q+1));
      	fi; # compactado o arquivo images.tar
        #language_pt.tar
      	if [ -d $LOCAL'/language/pt-BR' ] ; then
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.com_jce.ini' ] ; then
      			TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.com_jce.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.com_jce.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.com_jce.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.mod_lofarticlescroller.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.mod_lofarticlescroller.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.mod_lofarticleslideshow.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.mod_lofarticleslideshow.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_atomic.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_atomic.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_atomic.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_atomic.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez_20.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez_20.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez_20.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez_20.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez5.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez5.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez5.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez5.sys.ini";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #language_en.tar
      	if [ -d $LOCAL'/language/en-GB' ] ; then
      		if [ -e $LOCAL'/language/en-GB/en-GB.com_jce.ini' ] ; then
      			TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.com_jce.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.com_jce.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.com_jce.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.mod_lofarticlescroller.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.mod_lofarticlescroller.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.mod_lofarticleslideshow.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.mod_lofarticleslideshow.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_atomic.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_atomic.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_atomic.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_atomic.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez_20.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez_20.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez_20.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez_20.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez5.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez5.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez5.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez5.sys.ini";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #media.tar
      	if [ -d $LOCAL'/media/jce' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[6]:0} --absolute-names $LOCAL/media/jce";
      		Q=$(($Q+1));
      	fi;
        #modules.tar (OPCIONAL)
      	if [ -d $LOCAL'/modules/mod_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/modules/mod_lofarticlescroller' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_lofarticlescroller";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/modules/mod_lofarticleslideshow' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_lofarticleslideshow";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #plugins.tar (OPCIONAL)
      	if [ -d $LOCAL'/plugins/xmap' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[8]:0} --absolute-names $LOCAL/plugins/xmap";
      		Q=$(($Q+1));
      	fi;
        #templates.tar
      	if [ -d $LOCAL'/templates/joomtic3' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/templates/atomic' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/atomic";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/templates/beez_20' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/beez_20";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/templates/beez5' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/beez5";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #extras.tar
      	if [ -e $LOCAL'/configuration.php' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[10]:0} --absolute-names $LOCAL/configuration.php";
      		Q=$(($Q+1));
      		# Uso futuro
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/css/cores.css";
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/images/banner_logo.jpg";
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/variaveis.php";
      	fi;
      	sleep 2;
      	clear;
        # Tela -----------------------------------------
        echo "Passo 9 de 13 - Preservando parte da estrutura original...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        # Execução da compactação
        X=1;
        for (( i = 0; i < $Q; i++ ));
        do
          echo "[Aguarde... Compactando arquivos no diretório temporário. Fazendo:$X/$Q]";
        	echo `${TARCMD[$i]:0}`;
          X=$(($X+1));
        	sleep 1;
        	clear;
        done;
        # Tela -----------------------------------------
        echo "Passo 10 de 13 - Removendo estrutura de pastas e arquivos atuais...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        # Exclusão da estrutura de diretórios da instância atual
        echo "[Aguarde... Excluindo pastas e subpastas da instância atual... ]";
        rm -rf $LOCAL;
        mkdir $LOCAL;
        sleep 5;
        clear;
        # Tela -----------------------------------------
        echo "Passo 11 de 13 - Criando a nova estrutura de pastas e arquivos...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        # Criação da nova estrutura
        if [ -e $FILESTRU ] ; then
      				if [ -d $LOCAL ]; then
      						echo "[Aguarde... Recriando pastas e subpastas para a instância atual... ]";
      						echo `tar -xf $FILESTRU -C $LOCAL`;
      						echo `rm -rf $LOCAL/installation`;
      				else
      						echo "[ERRO ! O local não existe. O script não pode recriar a estrutura ! ]";
      				fi;
      				sleep 5;
      				clear;
      				echo "[ Estrutura criada ! Prosseguindo... ]";
      	else
      		echo "[ERRO ! O arquivo $FILESTRU requerido para descompactar a estrutura nova não foi encontrado ! ]";
      		exit 3;
      	fi;
        # Tela -----------------------------------------
        echo "Passo 12 de 13 - Criando a lista de arquivos para descompactar...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        # Recriando a lista de arquivos para descompactar
        DESCOMP=$TMPFILES"/descompactados.txt";
        if [ -e $DESCOMP ] ; then
              rm -rf $DESCOMP;
        else
              X=1;
              echo "[Aguarde... Gerando o arquivo com a lista de arquivos a descompactar... ]";
              sleep 2;
              echo ${DESTFILE[$X]:0} > $DESCOMP;
              while [ $X -lt 11 ]
              do
                  X=$(($X+1));
                  echo ${DESTFILE[$X]:0} >> $DESCOMP;
              done;
              echo "[ Arquivo $DESCOMP gerado. Prosseguindo... ]";
              sleep 5;
              clear;
        fi;
        # Tela -----------------------------------------
        echo "Passo 13 de 13 - Mesclando arquivos e pastas originais para a nova estrutura...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        echo "[12] - Criada a lista de arquivos para descompactação.";
        # Mesclando arquivos e pastas antigos com a nova estrutura
        if [ -e $DESCOMP ]; then
          COMPLIST=`cat ${DESCOMP}`;
          # Rotina de recolocação dos arquivos compactados para mesclar com a nova estrutura
          X=0;
          for f in $COMPLIST
          do
            X=$(($X+1));
            VET[$X]=$f;
          done;
          Z=$X;
          while [ $X -gt 0 ]; do
            echo "[ Recolocando estrutura física (Diretórios e arquivos). Restam: $X/$Z ]";
            if [ -e "$TMPFILES/${VET[$X]:0}" ]; then
              echo "[ Aguarde... Descompactando o arquivo ${VET[$X]:0} na estrutura existente... ]";
              if [ "${VET[$X]:0}" == "extras.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/configuration.php,configuration.php,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "templates.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/templates,templates/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "plugins.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/plugins,plugins/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "modules.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/modules,modules/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "media.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/media,media/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "language_en.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/language/en-GB,language/en-GB/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "language_pt.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/language/pt-BR,language/pt-BR/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "images.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/images,images/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "components.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/components,components/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "administrator.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/administrator,administrator/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              fi;
            fi;
            X=$(($X-1));
            sleep 1;
            clear;
          done;
          # Apagando a lista de descompatação ao sair
          rm -rf $DESCOMP;
          clear;
        else
          echo "[ERRO ! O arquivo $DESCOMP requerido para identificar os arquivos compactados não foi encontrado ! ]";
          exit 4;
        fi; # verificação da lista de descompactação
        # Tela -----------------------------------------
        echo "Passo 13 de 13 - Mesclagem concluída. Finalizado !";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        echo "[12] - Criada a lista de arquivos para descompactação.";
        echo "[13] - Mesclagem concluída. Processo finalizado !"
        echo "[ ... Fim do processo de atualização da instância: $INSTANCIA ]";
        echo "[ ... Aguardando para fazer a próxima instância... ]";
        sleep 5;
        clear;
      else
        echo "[ERRO. O local de destino não existe. Nada a fazer !]";
        exit 5;
      fi; # verificação do local da instância
      # ------------------------------------------------------------------------------------------------------------------------------ REMOVER
    fi; # verificação de tabelas
  fi; # versão é 3
	if [ $CAPTURA -lt 3 ]; then
		echo "[ AVISO: A instância #$INSTANCIA não é compatível com este script. ]";
    sleep 2;
	fi; # a versão não é 3
  INSTANCIA=$((INSTANCIA+1));
done; # instâncias não atingiram o total
echo "------ FIM DO SCRIPT ----------------------------------------------------------------";#!/bin/bash

# SCRIPT SHELL para migrar versão Joomla para 3.7
# Divisão de Portais Web - spw@tic.ufrj.br
# TIC/UFRJ - Todos os direitos reservados
# Criado em: 08/05/2017
# ----------------------------------------------
# Use: ./migra_37

# variáveis globais ----------------------------

# local onde se espera encontrar as instâncias Joomla
BASEPATH="/var/www/html/area_script";
# local onde ficarão armazenados todos os arquivos de backup de dump das instâncias processadas. ex. /home/ticadmin
PATHBACKUP="/home/dennis/Downloads";
# localização do arquivo contendo o pacote da versão 3.7 do Joomla
FILESTRU=`find ${PATHBACKUP} -type f -name 'Joomla_3.7.0-Stable-Full_Package.tar.gz'`;
# Nome do arquivo contido no dump original (padrão)
ARQUIVO='joomla.sql';
# Nome do arquivo base gerado
MODFILE='migra.sql';
# Local de extração padrão do TAR
SUBDIR='installation/sql/mysql/';
# Prefixo padrão das tabelas contidas no dump original
PREFIX='#__';
# Coletando a data atual para usar como parte do nome do arquivo de backup
DATE=`date +%Y-%m-%d`;
# Detectando a versão Joomla existente
COMANDO=`find ${BASEPATH} -type f -name 'configuration.php' ! -wholename '**/administrator'`;

# Cabeçalho inicial
clear;
echo "***** SCRIPT SHELL para migrar versão Joomla para 3.7 *******";
echo "*************************************************************";
echo "*** AGUARDE. Detectando Joomlas no local especificado...";
echo "*************************************************************";
sleep 2;


# Bloco de detecção de versão
CONT=0;
for L in ${COMANDO} ;
do
  D=`dirname $L` ;
  DIR=$D/portal/language/en-GB/en-GB.xml;
  DIRE=$D/language/en-GB/en-GB.xml;
	if [[ -e "$DIR" ]] || [[ -e "$DIRE" ]] ; then
		if [[ -e "$DIR" ]] ; then
			RDIR=$DIR ;
			VERSAO=`grep 'version' $RDIR | sed '{3! d;s/<[^>]*>//g}'` ;
			VERSAOFINAL=`echo $VERSAO | cut -d "." -f 1`;
			#echo "[Achei JOOMLA] >> em $D na versão $VERSAO" ;
		elif [[ -e "$DIRE" ]] ; then
			RDIR=$DIRE ;
			VERSAO=`grep 'version' $RDIR | sed '{3! d;s/<[^>]*>//g}'` ;
			VERSAOFINAL=`echo $VERSAO | cut -d "." -f 1`;
			#echo "[Achei JOOMLA] >> em $D na versão $VERSAO" ;
		fi;
		PATHFOUND[$CONT]=$D;
		VERSIONFOUND[$CONT]=$VERSAOFINAL;
		CONT=$(($CONT+1));
	fi;
done;

TOTAL=$CONT;
clear;
echo "***** SCRIPT SHELL para migrar versão Joomla para 3.7 *******";
echo "[ Aguarde... Processando instâncias de zero até $TOTAL... ]";
sleep 2;

# Início do processamento das N instâncias encontradas
INSTANCIA=0;
while [ $INSTANCIA -lt $TOTAL ]
do
	# Configurações --------------------
  CAPTURA=${VERSIONFOUND[$INSTANCIA]}; # Variável que captura qualquer versão 3 detectada
  LOCAL=${PATHFOUND[$INSTANCIA]};      # Local onde serão armazenados os arquivos e pastas zipados
	MYSQL=$(which mysql);
	AWK=$(which awk);
	GREP=$(which grep);
	MYSQLDUMP=$(which mysqldump);
	if [ $CAPTURA -eq 3 ]; then
    # variáveis locais
    BD=`grep '$db = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    USUARIO=`grep '$user = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    PASS=`grep '$password = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    PREFIXB=`grep '$dbprefix =' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    # Para uso futuro...
    # FROMNAME=`grep '$fromname =' configuration.php | cut -d "'" -f 2` ;
    # METADESC=`grep '$MetaDesc =' configuration.php | cut -d "'" -f 2` ;
    # METAKEYS=`grep '$MetaKeys =' configuration.php | cut -d "'" -f 2` ;
    # LOGPATH=`grep '$log_path =' configuration.php | cut -d "'" -f 2` ;
    # TMPPATH=`grep '$tmp_path =' configuration.php | cut -d "'" -f 2` ;
    # SITENAME=`grep '$sitename =' configuration.php | cut -d "'" -f 2` ;
    # Verificação se existem tabelas no banco de dados desta instância
    TABELAS=$($MYSQL -u $USUARIO -p$PASS $BD -e 'show tables'| $AWK '{ print $1}' | $GREP -v '^Tables' );
    # Local temporário para guardar os arquivos compactados e base SQL
    TMPFILES=${PATHBACKUP}"/temporaryfiles";
    # Recriando o diretório temporário se ele já existir
    if [ -d "$TMPFILES" ]; then
        rm -rf $TMPFILES;
        mkdir $TMPFILES; # Recriando
    else
        mkdir $TMPFILES; # Fazendo pela primeira vez
    fi;
    if [ "$TABELAS" == "" ] ; then
		  echo "[ AVISO: A instância #$INSTANCIA não possui tabelas no banco de dados. ]";
      sleep 2;
    else
      echo "[ Processando instância $INSTANCIA em $LOCAL ]";
      echo "------------------------------------------------------------------------";
      sleep 2;
      # ------------------------------------------------------------------------------------------------------------------------------ REMOVER
      # Flag para configuração dos arquivos SQL utilizados
      ENDSCRIPT="NAO";
      if [ "$ENDSCRIPT" == "NAO" ] ; then                                         # Criação dos padrões necessários
        echo `tar -vxf $FILESTRU -C $TMPFILES $SUBDIR$ARQUIVO`;                   # Descompacta "joomla.sql" em sua árvore original
        echo `mv $TMPFILES/$SUBDIR$ARQUIVO $TMPFILES`;                            # Move o arquivo "joomla.sql" para o diretório temporário
        echo `rm -rf $TMPFILES/$SUBDIR`;                                          # Apaga a árvore original
        echo `sed s/$PREFIX/$PREFIXB/g $TMPFILES/$ARQUIVO > $TMPFILES/$MODFILE`;  # Clonagem de "Joomla.sql" para "migra.sql" já com prefixos corretos
        # Criação de arquivos temporários de cada instância
        DESTINO=$TMPFILES"/tabelas.txt";                                          # lista de tabelas que devem ser reservadas da versão antiga
        EXPORT=$TMPFILES"/export.sql";                                            # dump de parte das tabelas da nova versão, excluindo-se as tabelas da versão antiga
        TRUNCATELIST=$TMPFILES"/truncatelist.txt";                                # lista das tabelas originais que serão truncadas
        TABELASATUAIS=$TMPFILES"/tabelasatuais.txt";                              # nome para sobrescrita da lista de tabelas separada por vírgulas
        TABELASEXCLUIDAS=$TMPFILES"/tabelasexcluidas.txt";                        # lista de tabelas antigas a excluir
        BACKUP="/backup_"$BD"_"$DATE".sql";                                       # nome do arquivo de backup
        ENDSCRIPT="SIM";
      fi;
      # Criando lista das tabelas que serão injetadas no arquivo TABELAS.TXT
      echo "[ Passo 1 de 13 ] - Criando índice de tabelas necessárias...";
      echo "===============================================================================";
      echo $PREFIXB"user_profiles" > $DESTINO;
      echo $PREFIXB"usergroups" >> $DESTINO;
      echo $PREFIXB"user_usergroup_map" >> $DESTINO;
      echo $PREFIXB"users" >> $DESTINO;
      echo $PREFIXB"menu" >> $DESTINO;
      echo $PREFIXB"menu_types" >> $DESTINO;
      echo $PREFIXB"modules" >> $DESTINO;
      echo $PREFIXB"modules_menu" >> $DESTINO;
      echo $PREFIXB"content_types" >> $DESTINO;
      echo $PREFIXB"content" >> $DESTINO;
      echo $PREFIXB"contact_details" >> $DESTINO;
      echo $PREFIXB"categories" >> $DESTINO;
      echo $PREFIXB"assets" >> $DESTINO;
      echo $PREFIXB"extensions" >> $DESTINO;
      echo $PREFIXB"template_styles" >> $DESTINO;
      echo $PREFIXB"viewlevels" >> $DESTINO;
      sleep 5;
      clear;
      # Tela ---------------------------------------------
      echo "[ Passo 2 de 13 ] - Criando o dump do banco de dados atual...";
      echo "=============================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO [OK]";
      sleep 5;
      clear;
      # Fazer o backup total do original e reservando-o
      $MYSQLDUMP -u $USUARIO -p$PASS $BD > ${PATHBACKUP}$BACKUP;
      clear;
      # Tela ---------------------------------------------
      echo "[ Passo 3 de 13 ] - Coletando de algumas tabelas, os dados importantes...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      sleep 5;
      clear;
      # Coletando algumas tabelas desse banco
      BUSCA=`cat $DESTINO`;
      INDICE=0;
      for i in $BUSCA
      do
       # Vetor
       INDICE=$(($INDICE+1));
       VETOR[$INDICE]=$i;
       TAMVETOR=${#VETOR[@]};
      done;
      # Coleta concluída
      $MYSQLDUMP -u $USUARIO -p$PASS $BD ${VETOR[@]:0:$INDICE} > $EXPORT;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 4 de 13 ] - Removendo todas as tabelas atuais...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      sleep 5;
      clear;
      # Apagando TODAS as tabelas
      echo $TABELAS >> $TABELASATUAIS;
      EXCLUSAO=`cat $TABELASATUAIS | tr ' ' ',' > $TABELASEXCLUIDAS`;
      EXCLUIDAS=`cat $TABELASEXCLUIDAS`;
      if [ "$EXCLUIDAS" == "" ]; then
          echo "[ ERRO. Não é possível ler o arquivo $TABELASEXCLUIDAS. Abortando... ";
          exit 1;
      fi;
      X=0;
      for f in $EXCLUIDAS
      do
        X=$(($X+1));
        VET[$X]=$f;
      done;
      while [ $X -gt 0 ]
      do
        echo "[ Aguarde... Apagando tabelas da instância: $X em $LOCAL ]";
        #echo "Tabelas atuais: ${VET[$X]:0};";
        $MYSQL -s -u $USUARIO -p$PASS $BD -e "DROP TABLE ${VET[$X]:0};";
        X=$(($X-1));
      done;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 5 de 13 ] - Recriando no banco a nova estrutura de tabelas.";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      sleep 5;
      clear;
      # Criando a nova estrutura sem as tabelas da lista
      FILE=`find ${TMPFILES} -type f -name $MODFILE`;    # localização do arquivo base gerado por este script (DEVE SER O MESMO EM MODFILE)
      if [ "$FILE" != "" ] ; then
            $MYSQL -u $USUARIO -p$PASS $BD < $FILE;
      fi;
      if [ "$FILE" == "" ] ; then
            echo "ERRO ! O arquivo requerido $MODFILE está faltando para executar este passo ! Abortando...";
            exit 2;
      fi;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 6 de 13 ] - Truncando as tabelas originais...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados."
      sleep 5;
      clear;
      # Preparação para truncar tabelas
      if [ -e $TRUNCATELIST ] ; then
          rm -rf $TRUNCATELIST;
      fi;
      echo $PREFIXB"user_profiles" > $TRUNCATELIST;
      echo $PREFIXB"usergroups" >> $TRUNCATELIST;
      echo $PREFIXB"user_usergroup_map" >> $TRUNCATELIST;
      echo $PREFIXB"users" >> $TRUNCATELIST;
      echo $PREFIXB"menu" >> $TRUNCATELIST;
      echo $PREFIXB"menu_types" >> $TRUNCATELIST;
      echo $PREFIXB"modules" >> $TRUNCATELIST;
      echo $PREFIXB"modules_menu" >> $TRUNCATELIST;
      echo $PREFIXB"content_types" >> $TRUNCATELIST;
      echo $PREFIXB"content" >> $TRUNCATELIST;
      echo $PREFIXB"contact_details" >> $TRUNCATELIST;
      echo $PREFIXB"categories" >> $TRUNCATELIST;
      echo $PREFIXB"assets" >> $TRUNCATELIST;
      echo $PREFIXB"extensions" >> $TRUNCATELIST;
      echo $PREFIXB"template_styles" >> $TRUNCATELIST;
      echo $PREFIXB"viewlevels" >> $TRUNCATELIST;
      # truncando as tabelas originais
      if [ -e "$TRUNCATELIST" ]; then
           TRUNCLIST=`cat $TRUNCATELIST`;
           X=0;
           for f in $TRUNCLIST
           do
               X=$(($X+1));
               VET[$X]=$f;
           done;
           Z=$X;
           while [ $X -gt 0 ]
           do
               echo "[Aguarde... Truncando tabelas originais da instância. Restam: $X/$Z ]";
               echo "Tabela atual: ${VET[$X]:0};";
               echo `$MYSQL -s -u $USUARIO -p$PASS $BD -e "TRUNCATE TABLE ${VET[$X]:0};"`;
               X=$(($X-1));
               sleep 2;
               clear;
           done;
      fi;
      sleep 5;
      clear;
      # Recolocando as tabelas da lista na nova estrutura
      # Tela -----------------------------------------
      echo "[ Passo 7 de 13 ] - Recolocando os dados da instância na nova estrutura...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados.";
      echo "[6] - Truncadas as tabelas originais.";
      sleep 5;
      clear;
      if [ -e "$EXPORT" ]; then
          echo "[Aguarde... Subindo o arquivo SQL... ]";
          $MYSQL -u $USUARIO -p$PASS $BD < $EXPORT;
          sleep 2;
          clear;
      fi;
      # Tela -----------------------------------------
      echo "[ Passo 8 de 13 ] - Removendo arquivos temporários...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados."
      echo "[6] - Truncadas as tabelas originais.";
      echo "[7] - Recolocado em $BD os dados guardados.";
      sleep 5;
      clear;
      # limpando arquivos temporários (parte de banco de dados)
      if [ -e "$DESTINO" ] ; then rm -rf "$DESTINO"; fi;
      if [ -e "$EXPORT" ] ; then rm -rf "$EXPORT"; fi;
      if [ -e "$TRUNCATELIST" ] ; then rm -rf "$TRUNCATELIST"; fi;
      if [ -e "$TABELASATUAIS" ] ; then rm -rf "$TABELASATUAIS"; fi;
      # limpando arquivos temporários (parte estrutural - 10 arquivos)
      if [ -e "$TMPFILES/${DESTFILE[1]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[1]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[2]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[2]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[3]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[3]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[4]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[4]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[5]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[5]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[6]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[6]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[7]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[7]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[8]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[8]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[9]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[9]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[10]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[10]:0}"; fi;
      # Nome dos arquivos usados na preservação de parte da estrutura original
      DESTFILE[1]="administrator.tar";
      DESTFILE[2]="components.tar";
      DESTFILE[3]="images.tar";
      DESTFILE[4]="language_pt.tar";
      DESTFILE[5]="language_en.tar";
      DESTFILE[6]="media.tar";
      DESTFILE[7]="modules.tar";
      DESTFILE[8]="plugins.tar";
      DESTFILE[9]="templates.tar";
      DESTFILE[10]="extras.tar";
      # Verificação do diretório temporário
      if [ -d $TMPFILES ]; then
      	echo "Compactando...";
      else
      	mkdir $TMPFILES;
      fi;
      if [ -d $LOCAL ]; then
        echo "[Aguarde... Verificando arquivos e pastas para compactar... ]";
      	sleep 2;
      	Q=0;
        # administrator.tar
      	if [ -d $LOCAL'/administrator/components/com_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/components/com_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/administrator/components/com_jce' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/components/com_jce";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/administrator/language/pt-BR' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/language/pt-BR";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/manifests/packages/pkg_pt-BR.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/manifests/packages/pkg_pt-BR.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/manifests/packages/pkg_weblinks.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/manifests/packages/pkg_weblinks.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/templates/bluestork' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/templates/bluestork";
      			Q=$(($Q+1));
      		fi;
      	fi; # Compactado o arquivo administrator.tar
        #components.tar
      	if [ -d $LOCAL'/components/com_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[2]:0} --absolute-names $LOCAL/components/com_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/components/com_jce' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[2]:0} --absolute-names $LOCAL/components/com_jce";
      			Q=$(($Q+1));
      		fi;
      	fi; # Compactado o arquivo componentes.tar
        #images.tar
      	if [ -d $LOCAL'/images' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[3]:0} --absolute-names $LOCAL/images";
      		Q=$(($Q+1));
      	fi; # compactado o arquivo images.tar
        #language_pt.tar
      	if [ -d $LOCAL'/language/pt-BR' ] ; then
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.com_jce.ini' ] ; then
      			TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.com_jce.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.com_jce.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.com_jce.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.mod_lofarticlescroller.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.mod_lofarticlescroller.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.mod_lofarticleslideshow.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.mod_lofarticleslideshow.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_atomic.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_atomic.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_atomic.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_atomic.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez_20.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez_20.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez_20.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez_20.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez5.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez5.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez5.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez5.sys.ini";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #language_en.tar
      	if [ -d $LOCAL'/language/en-GB' ] ; then
      		if [ -e $LOCAL'/language/en-GB/en-GB.com_jce.ini' ] ; then
      			TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.com_jce.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.com_jce.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.com_jce.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.mod_lofarticlescroller.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.mod_lofarticlescroller.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.mod_lofarticleslideshow.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.mod_lofarticleslideshow.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_atomic.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_atomic.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_atomic.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_atomic.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez_20.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez_20.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez_20.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez_20.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez5.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez5.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez5.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez5.sys.ini";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #media.tar
      	if [ -d $LOCAL'/media/jce' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[6]:0} --absolute-names $LOCAL/media/jce";
      		Q=$(($Q+1));
      	fi;
        #modules.tar (OPCIONAL)
      	if [ -d $LOCAL'/modules/mod_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/modules/mod_lofarticlescroller' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_lofarticlescroller";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/modules/mod_lofarticleslideshow' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_lofarticleslideshow";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #plugins.tar (OPCIONAL)
      	if [ -d $LOCAL'/plugins/xmap' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[8]:0} --absolute-names $LOCAL/plugins/xmap";
      		Q=$(($Q+1));
      	fi;
        #templates.tar
      	if [ -d $LOCAL'/templates/joomtic3' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/templates/atomic' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/atomic";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/templates/beez_20' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/beez_20";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/templates/beez5' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/beez5";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #extras.tar
      	if [ -e $LOCAL'/configuration.php' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[10]:0} --absolute-names $LOCAL/configuration.php";
      		Q=$(($Q+1));
      		# Uso futuro
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/css/cores.css";
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/images/banner_logo.jpg";
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/variaveis.php";
      	fi;
      	sleep 2;
      	clear;
        # Tela -----------------------------------------
        echo "Passo 9 de 13 - Preservando parte da estrutura original...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        # Execução da compactação
        X=1;
        for (( i = 0; i < $Q; i++ ));
        do
          echo "[Aguarde... Compactando arquivos no diretório temporário. Fazendo:$X/$Q]";
        	echo `${TARCMD[$i]:0}`;
          X=$(($X+1));
        	sleep 1;
        	clear;
        done;
        # Tela -----------------------------------------
        echo "Passo 10 de 13 - Removendo estrutura de pastas e arquivos atuais...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        # Exclusão da estrutura de diretórios da instância atual
        echo "[Aguarde... Excluindo pastas e subpastas da instância atual... ]";
        rm -rf $LOCAL;
        mkdir $LOCAL;
        sleep 5;
        clear;
        # Tela -----------------------------------------
        echo "Passo 11 de 13 - Criando a nova estrutura de pastas e arquivos...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        # Criação da nova estrutura
        if [ -e $FILESTRU ] ; then
      				if [ -d $LOCAL ]; then
      						echo "[Aguarde... Recriando pastas e subpastas para a instância atual... ]";
      						echo `tar -xf $FILESTRU -C $LOCAL`;
      						echo `rm -rf $LOCAL/installation`;
      				else
      						echo "[ERRO ! O local não existe. O script não pode recriar a estrutura ! ]";
      				fi;
      				sleep 5;
      				clear;
      				echo "[ Estrutura criada ! Prosseguindo... ]";
      	else
      		echo "[ERRO ! O arquivo $FILESTRU requerido para descompactar a estrutura nova não foi encontrado ! ]";
      		exit 3;
      	fi;
        # Tela -----------------------------------------
        echo "Passo 12 de 13 - Criando a lista de arquivos para descompactar...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        # Recriando a lista de arquivos para descompactar
        DESCOMP=$TMPFILES"/descompactados.txt";
        if [ -e $DESCOMP ] ; then
              rm -rf $DESCOMP;
        else
              X=1;
              echo "[Aguarde... Gerando o arquivo com a lista de arquivos a descompactar... ]";
              sleep 2;
              echo ${DESTFILE[$X]:0} > $DESCOMP;
              while [ $X -lt 11 ]
              do
                  X=$(($X+1));
                  echo ${DESTFILE[$X]:0} >> $DESCOMP;
              done;
              echo "[ Arquivo $DESCOMP gerado. Prosseguindo... ]";
              sleep 5;
              clear;
        fi;
        # Tela -----------------------------------------
        echo "Passo 13 de 13 - Mesclando arquivos e pastas originais para a nova estrutura...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        echo "[12] - Criada a lista de arquivos para descompactação.";
        # Mesclando arquivos e pastas antigos com a nova estrutura
        if [ -e $DESCOMP ]; then
          COMPLIST=`cat ${DESCOMP}`;
          # Rotina de recolocação dos arquivos compactados para mesclar com a nova estrutura
          X=0;
          for f in $COMPLIST
          do
            X=$(($X+1));
            VET[$X]=$f;
          done;
          Z=$X;
          while [ $X -gt 0 ]; do
            echo "[ Recolocando estrutura física (Diretórios e arquivos). Restam: $X/$Z ]";
            if [ -e "$TMPFILES/${VET[$X]:0}" ]; then
              echo "[ Aguarde... Descompactando o arquivo ${VET[$X]:0} na estrutura existente... ]";
              if [ "${VET[$X]:0}" == "extras.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/configuration.php,configuration.php,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "templates.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/templates,templates/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "plugins.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/plugins,plugins/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "modules.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/modules,modules/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "media.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/media,media/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "language_en.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/language/en-GB,language/en-GB/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "language_pt.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/language/pt-BR,language/pt-BR/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "images.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/images,images/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "components.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/components,components/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "administrator.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/administrator,administrator/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              fi;
            fi;
            X=$(($X-1));
            sleep 1;
            clear;
          done;
          # Apagando a lista de descompatação ao sair
          rm -rf $DESCOMP;
          clear;
        else
          echo "[ERRO ! O arquivo $DESCOMP requerido para identificar os arquivos compactados não foi encontrado ! ]";
          exit 4;
        fi; # verificação da lista de descompactação
        # Tela -----------------------------------------
        echo "Passo 13 de 13 - Mesclagem concluída. Finalizado !";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        echo "[12] - Criada a lista de arquivos para descompactação.";
        echo "[13] - Mesclagem concluída. Processo finalizado !"
        echo "[ ... Fim do processo de atualização da instância: $INSTANCIA ]";
        echo "[ ... Aguardando para fazer a próxima instância... ]";
        sleep 5;
        clear;
      else
        echo "[ERRO. O local de destino não existe. Nada a fazer !]";
        exit 5;
      fi; # verificação do local da instância
      # ------------------------------------------------------------------------------------------------------------------------------ REMOVER
    fi; # verificação de tabelas
  fi; # versão é 3
	if [ $CAPTURA -lt 3 ]; then
		echo "[ AVISO: A instância #$INSTANCIA não é compatível com este script. ]";
    sleep 2;
	fi; # a versão não é 3
  INSTANCIA=$((INSTANCIA+1));
done; # instâncias não atingiram o total
echo "------ FIM DO SCRIPT ----------------------------------------------------------------";#!/bin/bash

# SCRIPT SHELL para migrar versão Joomla para 3.7
# Divisão de Portais Web - spw@tic.ufrj.br
# TIC/UFRJ - Todos os direitos reservados
# Criado em: 08/05/2017
# ----------------------------------------------
# Use: ./migra_37

# variáveis globais ----------------------------

# local onde se espera encontrar as instâncias Joomla
BASEPATH="/var/www/html/area_script";
# local onde ficarão armazenados todos os arquivos de backup de dump das instâncias processadas. ex. /home/ticadmin
PATHBACKUP="/home/dennis/Downloads";
# localização do arquivo contendo o pacote da versão 3.7 do Joomla
FILESTRU=`find ${PATHBACKUP} -type f -name 'Joomla_3.7.0-Stable-Full_Package.tar.gz'`;
# Nome do arquivo contido no dump original (padrão)
ARQUIVO='joomla.sql';
# Nome do arquivo base gerado
MODFILE='migra.sql';
# Local de extração padrão do TAR
SUBDIR='installation/sql/mysql/';
# Prefixo padrão das tabelas contidas no dump original
PREFIX='#__';
# Coletando a data atual para usar como parte do nome do arquivo de backup
DATE=`date +%Y-%m-%d`;
# Detectando a versão Joomla existente
COMANDO=`find ${BASEPATH} -type f -name 'configuration.php' ! -wholename '**/administrator'`;

# Cabeçalho inicial
clear;
echo "***** SCRIPT SHELL para migrar versão Joomla para 3.7 *******";
echo "*************************************************************";
echo "*** AGUARDE. Detectando Joomlas no local especificado...";
echo "*************************************************************";
sleep 2;


# Bloco de detecção de versão
CONT=0;
for L in ${COMANDO} ;
do
  D=`dirname $L` ;
  DIR=$D/portal/language/en-GB/en-GB.xml;
  DIRE=$D/language/en-GB/en-GB.xml;
	if [[ -e "$DIR" ]] || [[ -e "$DIRE" ]] ; then
		if [[ -e "$DIR" ]] ; then
			RDIR=$DIR ;
			VERSAO=`grep 'version' $RDIR | sed '{3! d;s/<[^>]*>//g}'` ;
			VERSAOFINAL=`echo $VERSAO | cut -d "." -f 1`;
			#echo "[Achei JOOMLA] >> em $D na versão $VERSAO" ;
		elif [[ -e "$DIRE" ]] ; then
			RDIR=$DIRE ;
			VERSAO=`grep 'version' $RDIR | sed '{3! d;s/<[^>]*>//g}'` ;
			VERSAOFINAL=`echo $VERSAO | cut -d "." -f 1`;
			#echo "[Achei JOOMLA] >> em $D na versão $VERSAO" ;
		fi;
		PATHFOUND[$CONT]=$D;
		VERSIONFOUND[$CONT]=$VERSAOFINAL;
		CONT=$(($CONT+1));
	fi;
done;

TOTAL=$CONT;
clear;
echo "***** SCRIPT SHELL para migrar versão Joomla para 3.7 *******";
echo "[ Aguarde... Processando instâncias de zero até $TOTAL... ]";
sleep 2;

# Início do processamento das N instâncias encontradas
INSTANCIA=0;
while [ $INSTANCIA -lt $TOTAL ]
do
	# Configurações --------------------
  CAPTURA=${VERSIONFOUND[$INSTANCIA]}; # Variável que captura qualquer versão 3 detectada
  LOCAL=${PATHFOUND[$INSTANCIA]};      # Local onde serão armazenados os arquivos e pastas zipados
	MYSQL=$(which mysql);
	AWK=$(which awk);
	GREP=$(which grep);
	MYSQLDUMP=$(which mysqldump);
	if [ $CAPTURA -eq 3 ]; then
    # variáveis locais
    BD=`grep '$db = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    USUARIO=`grep '$user = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    PASS=`grep '$password = ' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    PREFIXB=`grep '$dbprefix =' $LOCAL/configuration.php | cut -d "'" -f 2` ;
    # Para uso futuro...
    # FROMNAME=`grep '$fromname =' configuration.php | cut -d "'" -f 2` ;
    # METADESC=`grep '$MetaDesc =' configuration.php | cut -d "'" -f 2` ;
    # METAKEYS=`grep '$MetaKeys =' configuration.php | cut -d "'" -f 2` ;
    # LOGPATH=`grep '$log_path =' configuration.php | cut -d "'" -f 2` ;
    # TMPPATH=`grep '$tmp_path =' configuration.php | cut -d "'" -f 2` ;
    # SITENAME=`grep '$sitename =' configuration.php | cut -d "'" -f 2` ;
    # Verificação se existem tabelas no banco de dados desta instância
    TABELAS=$($MYSQL -u $USUARIO -p$PASS $BD -e 'show tables'| $AWK '{ print $1}' | $GREP -v '^Tables' );
    # Local temporário para guardar os arquivos compactados e base SQL
    TMPFILES=${PATHBACKUP}"/temporaryfiles";
    # Recriando o diretório temporário se ele já existir
    if [ -d "$TMPFILES" ]; then
        rm -rf $TMPFILES;
        mkdir $TMPFILES; # Recriando
    else
        mkdir $TMPFILES; # Fazendo pela primeira vez
    fi;
    if [ "$TABELAS" == "" ] ; then
		  echo "[ AVISO: A instância #$INSTANCIA não possui tabelas no banco de dados. ]";
      sleep 2;
    else
      echo "[ Processando instância $INSTANCIA em $LOCAL ]";
      echo "------------------------------------------------------------------------";
      sleep 2;
      # ------------------------------------------------------------------------------------------------------------------------------ REMOVER
      # Flag para configuração dos arquivos SQL utilizados
      ENDSCRIPT="NAO";
      if [ "$ENDSCRIPT" == "NAO" ] ; then                                         # Criação dos padrões necessários
        echo `tar -vxf $FILESTRU -C $TMPFILES $SUBDIR$ARQUIVO`;                   # Descompacta "joomla.sql" em sua árvore original
        echo `mv $TMPFILES/$SUBDIR$ARQUIVO $TMPFILES`;                            # Move o arquivo "joomla.sql" para o diretório temporário
        echo `rm -rf $TMPFILES/$SUBDIR`;                                          # Apaga a árvore original
        echo `sed s/$PREFIX/$PREFIXB/g $TMPFILES/$ARQUIVO > $TMPFILES/$MODFILE`;  # Clonagem de "Joomla.sql" para "migra.sql" já com prefixos corretos
        # Criação de arquivos temporários de cada instância
        DESTINO=$TMPFILES"/tabelas.txt";                                          # lista de tabelas que devem ser reservadas da versão antiga
        EXPORT=$TMPFILES"/export.sql";                                            # dump de parte das tabelas da nova versão, excluindo-se as tabelas da versão antiga
        TRUNCATELIST=$TMPFILES"/truncatelist.txt";                                # lista das tabelas originais que serão truncadas
        TABELASATUAIS=$TMPFILES"/tabelasatuais.txt";                              # nome para sobrescrita da lista de tabelas separada por vírgulas
        TABELASEXCLUIDAS=$TMPFILES"/tabelasexcluidas.txt";                        # lista de tabelas antigas a excluir
        BACKUP="/backup_"$BD"_"$DATE".sql";                                       # nome do arquivo de backup
        ENDSCRIPT="SIM";
      fi;
      # Criando lista das tabelas que serão injetadas no arquivo TABELAS.TXT
      echo "[ Passo 1 de 13 ] - Criando índice de tabelas necessárias...";
      echo "===============================================================================";
      echo $PREFIXB"user_profiles" > $DESTINO;
      echo $PREFIXB"usergroups" >> $DESTINO;
      echo $PREFIXB"user_usergroup_map" >> $DESTINO;
      echo $PREFIXB"users" >> $DESTINO;
      echo $PREFIXB"menu" >> $DESTINO;
      echo $PREFIXB"menu_types" >> $DESTINO;
      echo $PREFIXB"modules" >> $DESTINO;
      echo $PREFIXB"modules_menu" >> $DESTINO;
      echo $PREFIXB"content_types" >> $DESTINO;
      echo $PREFIXB"content" >> $DESTINO;
      echo $PREFIXB"contact_details" >> $DESTINO;
      echo $PREFIXB"categories" >> $DESTINO;
      echo $PREFIXB"assets" >> $DESTINO;
      echo $PREFIXB"extensions" >> $DESTINO;
      echo $PREFIXB"template_styles" >> $DESTINO;
      echo $PREFIXB"viewlevels" >> $DESTINO;
      sleep 5;
      clear;
      # Tela ---------------------------------------------
      echo "[ Passo 2 de 13 ] - Criando o dump do banco de dados atual...";
      echo "=============================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO [OK]";
      sleep 5;
      clear;
      # Fazer o backup total do original e reservando-o
      $MYSQLDUMP -u $USUARIO -p$PASS $BD > ${PATHBACKUP}$BACKUP;
      clear;
      # Tela ---------------------------------------------
      echo "[ Passo 3 de 13 ] - Coletando de algumas tabelas, os dados importantes...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      sleep 5;
      clear;
      # Coletando algumas tabelas desse banco
      BUSCA=`cat $DESTINO`;
      INDICE=0;
      for i in $BUSCA
      do
       # Vetor
       INDICE=$(($INDICE+1));
       VETOR[$INDICE]=$i;
       TAMVETOR=${#VETOR[@]};
      done;
      # Coleta concluída
      $MYSQLDUMP -u $USUARIO -p$PASS $BD ${VETOR[@]:0:$INDICE} > $EXPORT;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 4 de 13 ] - Removendo todas as tabelas atuais...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      sleep 5;
      clear;
      # Apagando TODAS as tabelas
      echo $TABELAS >> $TABELASATUAIS;
      EXCLUSAO=`cat $TABELASATUAIS | tr ' ' ',' > $TABELASEXCLUIDAS`;
      EXCLUIDAS=`cat $TABELASEXCLUIDAS`;
      if [ "$EXCLUIDAS" == "" ]; then
          echo "[ ERRO. Não é possível ler o arquivo $TABELASEXCLUIDAS. Abortando... ";
          exit 1;
      fi;
      X=0;
      for f in $EXCLUIDAS
      do
        X=$(($X+1));
        VET[$X]=$f;
      done;
      while [ $X -gt 0 ]
      do
        echo "[ Aguarde... Apagando tabelas da instância: $X em $LOCAL ]";
        #echo "Tabelas atuais: ${VET[$X]:0};";
        $MYSQL -s -u $USUARIO -p$PASS $BD -e "DROP TABLE ${VET[$X]:0};";
        X=$(($X-1));
      done;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 5 de 13 ] - Recriando no banco a nova estrutura de tabelas.";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      sleep 5;
      clear;
      # Criando a nova estrutura sem as tabelas da lista
      FILE=`find ${TMPFILES} -type f -name $MODFILE`;    # localização do arquivo base gerado por este script (DEVE SER O MESMO EM MODFILE)
      if [ "$FILE" != "" ] ; then
            $MYSQL -u $USUARIO -p$PASS $BD < $FILE;
      fi;
      if [ "$FILE" == "" ] ; then
            echo "ERRO ! O arquivo requerido $MODFILE está faltando para executar este passo ! Abortando...";
            exit 2;
      fi;
      sleep 5;
      clear;
      # Tela -----------------------------------------
      echo "[ Passo 6 de 13 ] - Truncando as tabelas originais...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados."
      sleep 5;
      clear;
      # Preparação para truncar tabelas
      if [ -e $TRUNCATELIST ] ; then
          rm -rf $TRUNCATELIST;
      fi;
      echo $PREFIXB"user_profiles" > $TRUNCATELIST;
      echo $PREFIXB"usergroups" >> $TRUNCATELIST;
      echo $PREFIXB"user_usergroup_map" >> $TRUNCATELIST;
      echo $PREFIXB"users" >> $TRUNCATELIST;
      echo $PREFIXB"menu" >> $TRUNCATELIST;
      echo $PREFIXB"menu_types" >> $TRUNCATELIST;
      echo $PREFIXB"modules" >> $TRUNCATELIST;
      echo $PREFIXB"modules_menu" >> $TRUNCATELIST;
      echo $PREFIXB"content_types" >> $TRUNCATELIST;
      echo $PREFIXB"content" >> $TRUNCATELIST;
      echo $PREFIXB"contact_details" >> $TRUNCATELIST;
      echo $PREFIXB"categories" >> $TRUNCATELIST;
      echo $PREFIXB"assets" >> $TRUNCATELIST;
      echo $PREFIXB"extensions" >> $TRUNCATELIST;
      echo $PREFIXB"template_styles" >> $TRUNCATELIST;
      echo $PREFIXB"viewlevels" >> $TRUNCATELIST;
      # truncando as tabelas originais
      if [ -e "$TRUNCATELIST" ]; then
           TRUNCLIST=`cat $TRUNCATELIST`;
           X=0;
           for f in $TRUNCLIST
           do
               X=$(($X+1));
               VET[$X]=$f;
           done;
           Z=$X;
           while [ $X -gt 0 ]
           do
               echo "[Aguarde... Truncando tabelas originais da instância. Restam: $X/$Z ]";
               echo "Tabela atual: ${VET[$X]:0};";
               echo `$MYSQL -s -u $USUARIO -p$PASS $BD -e "TRUNCATE TABLE ${VET[$X]:0};"`;
               X=$(($X-1));
               sleep 2;
               clear;
           done;
      fi;
      sleep 5;
      clear;
      # Recolocando as tabelas da lista na nova estrutura
      # Tela -----------------------------------------
      echo "[ Passo 7 de 13 ] - Recolocando os dados da instância na nova estrutura...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados.";
      echo "[6] - Truncadas as tabelas originais.";
      sleep 5;
      clear;
      if [ -e "$EXPORT" ]; then
          echo "[Aguarde... Subindo o arquivo SQL... ]";
          $MYSQL -u $USUARIO -p$PASS $BD < $EXPORT;
          sleep 2;
          clear;
      fi;
      # Tela -----------------------------------------
      echo "[ Passo 8 de 13 ] - Removendo arquivos temporários...";
      echo "===============================================================================";
      echo "[1] - Arquivo índice de tabelas: $DESTINO";
      echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
      echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
      echo "[3] - Arquivo de tabelas em: $EXPORT";
      echo "[4] - Tabelas atuais apagadas de $BD.";
      echo "[5] - Recriada a nova estrutura do banco de dados."
      echo "[6] - Truncadas as tabelas originais.";
      echo "[7] - Recolocado em $BD os dados guardados.";
      sleep 5;
      clear;
      # limpando arquivos temporários (parte de banco de dados)
      if [ -e "$DESTINO" ] ; then rm -rf "$DESTINO"; fi;
      if [ -e "$EXPORT" ] ; then rm -rf "$EXPORT"; fi;
      if [ -e "$TRUNCATELIST" ] ; then rm -rf "$TRUNCATELIST"; fi;
      if [ -e "$TABELASATUAIS" ] ; then rm -rf "$TABELASATUAIS"; fi;
      # limpando arquivos temporários (parte estrutural - 10 arquivos)
      if [ -e "$TMPFILES/${DESTFILE[1]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[1]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[2]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[2]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[3]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[3]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[4]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[4]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[5]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[5]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[6]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[6]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[7]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[7]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[8]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[8]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[9]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[9]:0}"; fi;
      if [ -e "$TMPFILES/${DESTFILE[10]:0}" ] ; then rm -rf "$TMPFILES/${DESTFILE[10]:0}"; fi;
      # Nome dos arquivos usados na preservação de parte da estrutura original
      DESTFILE[1]="administrator.tar";
      DESTFILE[2]="components.tar";
      DESTFILE[3]="images.tar";
      DESTFILE[4]="language_pt.tar";
      DESTFILE[5]="language_en.tar";
      DESTFILE[6]="media.tar";
      DESTFILE[7]="modules.tar";
      DESTFILE[8]="plugins.tar";
      DESTFILE[9]="templates.tar";
      DESTFILE[10]="extras.tar";
      # Verificação do diretório temporário
      if [ -d $TMPFILES ]; then
      	echo "Compactando...";
      else
      	mkdir $TMPFILES;
      fi;
      if [ -d $LOCAL ]; then
        echo "[Aguarde... Verificando arquivos e pastas para compactar... ]";
      	sleep 2;
      	Q=0;
        # administrator.tar
      	if [ -d $LOCAL'/administrator/components/com_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/components/com_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/administrator/components/com_jce' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/components/com_jce";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/administrator/language/pt-BR' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/language/pt-BR";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/manifests/packages/pkg_pt-BR.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/manifests/packages/pkg_pt-BR.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/manifests/packages/pkg_weblinks.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/manifests/packages/pkg_weblinks.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/administrator/templates/bluestork' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[1]:0} --absolute-names $LOCAL/administrator/templates/bluestork";
      			Q=$(($Q+1));
      		fi;
      	fi; # Compactado o arquivo administrator.tar
        #components.tar
      	if [ -d $LOCAL'/components/com_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[2]:0} --absolute-names $LOCAL/components/com_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/components/com_jce' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[2]:0} --absolute-names $LOCAL/components/com_jce";
      			Q=$(($Q+1));
      		fi;
      	fi; # Compactado o arquivo componentes.tar
        #images.tar
      	if [ -d $LOCAL'/images' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[3]:0} --absolute-names $LOCAL/images";
      		Q=$(($Q+1));
      	fi; # compactado o arquivo images.tar
        #language_pt.tar
      	if [ -d $LOCAL'/language/pt-BR' ] ; then
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.com_jce.ini' ] ; then
      			TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.com_jce.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.com_jce.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.com_jce.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.mod_lofarticlescroller.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.mod_lofarticlescroller.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.mod_lofarticleslideshow.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.mod_lofarticleslideshow.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_atomic.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_atomic.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_atomic.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_atomic.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez_20.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez_20.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez_20.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez_20.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez5.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez5.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/pt-BR/pt-BR.tpl_beez5.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[4]:0} --absolute-names $LOCAL/language/pt-BR/pt-BR.tpl_beez5.sys.ini";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #language_en.tar
      	if [ -d $LOCAL'/language/en-GB' ] ; then
      		if [ -e $LOCAL'/language/en-GB/en-GB.com_jce.ini' ] ; then
      			TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.com_jce.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.com_jce.xml' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.com_jce.xml";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.mod_lofarticlescroller.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.mod_lofarticlescroller.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.mod_lofarticleslideshow.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.mod_lofarticleslideshow.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_atomic.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_atomic.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_atomic.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_atomic.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez_20.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez_20.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez_20.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez_20.sys.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez5.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez5.ini";
      			Q=$(($Q+1));
      		fi;
      		if [ -e $LOCAL'/language/en-GB/en-GB.tpl_beez5.sys.ini' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[5]:0} --absolute-names $LOCAL/language/en-GB/en-GB.tpl_beez5.sys.ini";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #media.tar
      	if [ -d $LOCAL'/media/jce' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[6]:0} --absolute-names $LOCAL/media/jce";
      		Q=$(($Q+1));
      	fi;
        #modules.tar (OPCIONAL)
      	if [ -d $LOCAL'/modules/mod_weblinks' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_weblinks";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/modules/mod_lofarticlescroller' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_lofarticlescroller";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/modules/mod_lofarticleslideshow' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[7]:0} --absolute-names $LOCAL/modules/mod_lofarticleslideshow";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #plugins.tar (OPCIONAL)
      	if [ -d $LOCAL'/plugins/xmap' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[8]:0} --absolute-names $LOCAL/plugins/xmap";
      		Q=$(($Q+1));
      	fi;
        #templates.tar
      	if [ -d $LOCAL'/templates/joomtic3' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3";
      		Q=$(($Q+1));
      		if [ -d $LOCAL'/templates/atomic' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/atomic";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/templates/beez_20' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/beez_20";
      			Q=$(($Q+1));
      		fi;
      		if [ -d $LOCAL'/templates/beez5' ] ; then
      			TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/beez5";
      			Q=$(($Q+1));
      		fi;
      	fi;
        #extras.tar
      	if [ -e $LOCAL'/configuration.php' ] ; then
      		TARCMD[$Q]="tar -cf $TMPFILES/${DESTFILE[10]:0} --absolute-names $LOCAL/configuration.php";
      		Q=$(($Q+1));
      		# Uso futuro
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/css/cores.css";
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/images/banner_logo.jpg";
      		#Q=$(($Q+1));
      		#TARCMD[$Q]="tar -rf $TMPFILES/${DESTFILE[9]:0} --absolute-names $LOCAL/templates/joomtic3/variaveis.php";
      	fi;
      	sleep 2;
      	clear;
        # Tela -----------------------------------------
        echo "Passo 9 de 13 - Preservando parte da estrutura original...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        # Execução da compactação
        X=1;
        for (( i = 0; i < $Q; i++ ));
        do
          echo "[Aguarde... Compactando arquivos no diretório temporário. Fazendo:$X/$Q]";
        	echo `${TARCMD[$i]:0}`;
          X=$(($X+1));
        	sleep 1;
        	clear;
        done;
        # Tela -----------------------------------------
        echo "Passo 10 de 13 - Removendo estrutura de pastas e arquivos atuais...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        # Exclusão da estrutura de diretórios da instância atual
        echo "[Aguarde... Excluindo pastas e subpastas da instância atual... ]";
        rm -rf $LOCAL;
        mkdir $LOCAL;
        sleep 5;
        clear;
        # Tela -----------------------------------------
        echo "Passo 11 de 13 - Criando a nova estrutura de pastas e arquivos...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        # Criação da nova estrutura
        if [ -e $FILESTRU ] ; then
      				if [ -d $LOCAL ]; then
      						echo "[Aguarde... Recriando pastas e subpastas para a instância atual... ]";
      						echo `tar -xf $FILESTRU -C $LOCAL`;
      						echo `rm -rf $LOCAL/installation`;
      				else
      						echo "[ERRO ! O local não existe. O script não pode recriar a estrutura ! ]";
      				fi;
      				sleep 5;
      				clear;
      				echo "[ Estrutura criada ! Prosseguindo... ]";
      	else
      		echo "[ERRO ! O arquivo $FILESTRU requerido para descompactar a estrutura nova não foi encontrado ! ]";
      		exit 3;
      	fi;
        # Tela -----------------------------------------
        echo "Passo 12 de 13 - Criando a lista de arquivos para descompactar...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        # Recriando a lista de arquivos para descompactar
        DESCOMP=$TMPFILES"/descompactados.txt";
        if [ -e $DESCOMP ] ; then
              rm -rf $DESCOMP;
        else
              X=1;
              echo "[Aguarde... Gerando o arquivo com a lista de arquivos a descompactar... ]";
              sleep 2;
              echo ${DESTFILE[$X]:0} > $DESCOMP;
              while [ $X -lt 11 ]
              do
                  X=$(($X+1));
                  echo ${DESTFILE[$X]:0} >> $DESCOMP;
              done;
              echo "[ Arquivo $DESCOMP gerado. Prosseguindo... ]";
              sleep 5;
              clear;
        fi;
        # Tela -----------------------------------------
        echo "Passo 13 de 13 - Mesclando arquivos e pastas originais para a nova estrutura...";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        echo "[12] - Criada a lista de arquivos para descompactação.";
        # Mesclando arquivos e pastas antigos com a nova estrutura
        if [ -e $DESCOMP ]; then
          COMPLIST=`cat ${DESCOMP}`;
          # Rotina de recolocação dos arquivos compactados para mesclar com a nova estrutura
          X=0;
          for f in $COMPLIST
          do
            X=$(($X+1));
            VET[$X]=$f;
          done;
          Z=$X;
          while [ $X -gt 0 ]; do
            echo "[ Recolocando estrutura física (Diretórios e arquivos). Restam: $X/$Z ]";
            if [ -e "$TMPFILES/${VET[$X]:0}" ]; then
              echo "[ Aguarde... Descompactando o arquivo ${VET[$X]:0} na estrutura existente... ]";
              if [ "${VET[$X]:0}" == "extras.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/configuration.php,configuration.php,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "templates.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/templates,templates/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "plugins.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/plugins,plugins/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "modules.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/modules,modules/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "media.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/media,media/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "language_en.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/language/en-GB,language/en-GB/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "language_pt.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/language/pt-BR,language/pt-BR/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "images.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/images,images/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "components.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/components,components/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              elif [ "${VET[$X]:0}" == "administrator.tar" ]; then
                echo `tar --transform='s,'$LOCAL'/administrator,administrator/,' -xf $TMPFILES/${VET[$X]:0} --absolute-names --directory $LOCAL`;
              fi;
            fi;
            X=$(($X-1));
            sleep 1;
            clear;
          done;
          # Apagando a lista de descompatação ao sair
          rm -rf $DESCOMP;
          clear;
        else
          echo "[ERRO ! O arquivo $DESCOMP requerido para identificar os arquivos compactados não foi encontrado ! ]";
          exit 4;
        fi; # verificação da lista de descompactação
        # Tela -----------------------------------------
        echo "Passo 13 de 13 - Mesclagem concluída. Finalizado !";
        echo "===============================================================================";
        echo "[1] - Arquivo índice de tabelas: $DESTINO";
        echo "[2] - Arquivo de backup: ${PATHBACKUP}$BACKUP";
        echo "[3] - Qtde. de tabelas coletadas: $TAMVETOR";
        echo "[3] - Arquivo de tabelas em: $EXPORT";
        echo "[4] - Tabelas originais apagadas.";
        echo "[5] - Recriada a nova estrutura do banco de dados."
        echo "[6] - Copiado para $BD as novas tabelas.";
        echo "[7] - Recolocado em $BD os dados guardados.";
        echo "[8] - Arquivos temporários removidos.";
        echo "[9] - Parte da estrutura original preservada.";
        echo "[10] - Estrutura original de pastas e arquivos removida.";
        echo "[11] - Criada a nova estrutura de pastas e arquivos.";
        echo "[12] - Criada a lista de arquivos para descompactação.";
        echo "[13] - Mesclagem concluída. Processo finalizado !"
        echo "[ ... Fim do processo de atualização da instância: $INSTANCIA ]";
        echo "[ ... Aguardando para fazer a próxima instância... ]";
        sleep 5;
        clear;
      else
        echo "[ERRO. O local de destino não existe. Nada a fazer !]";
        exit 5;
      fi; # verificação do local da instância
      # ------------------------------------------------------------------------------------------------------------------------------ REMOVER
    fi; # verificação de tabelas
  fi; # versão é 3
	if [ $CAPTURA -lt 3 ]; then
		echo "[ AVISO: A instância #$INSTANCIA não é compatível com este script. ]";
    sleep 2;
	fi; # a versão não é 3
  INSTANCIA=$((INSTANCIA+1));
done; # instâncias não atingiram o total
echo "------ FIM DO SCRIPT ----------------------------------------------------------------";
